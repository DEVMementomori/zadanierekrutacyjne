﻿using ZadanieRekrutacyjne.Data.Orders.Filters;
using ZadanieRekrutacyjne.Data.Orders.Models;

namespace ZadanieRekrutacyjne.Services.Orders
{
    public interface IOrderService
    {
        Task<List<Order>> GetOrdersAsync(OrdersFilter filter);
    }
}
