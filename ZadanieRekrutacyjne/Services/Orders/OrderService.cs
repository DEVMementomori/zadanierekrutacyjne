﻿using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;
using ZadanieRekrutacyjne.Data.Orders.Filters;
using ZadanieRekrutacyjne.Data.Orders.Models;
using ZadanieRekrutacyjne.Data.Orders.CSVMapper;
using System.Text;

namespace ZadanieRekrutacyjne.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly string _csvFilePath;

        public OrderService(IConfiguration configuration)
        {
            _csvFilePath = configuration.GetValue<string>("CsvFilePath");
        }

        public async Task<List<Order>> GetOrdersAsync(OrdersFilter filter)
        {
            if (_csvFilePath == null || !File.Exists(_csvFilePath))
            {
                return new List<Order>();
            }

            var configuration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HeaderValidated = null,
                MissingFieldFound = null,
                HasHeaderRecord = true,
                Delimiter = ",",
                Encoding = Encoding.UTF8
            };

            using var reader = new StreamReader(_csvFilePath);
            using var csv = new CsvReader(reader, configuration);
            csv.Context.RegisterClassMap<OrderMap>();

            var records = await csv.GetRecordsAsync<Order>()
                .Where(p =>
                    (string.IsNullOrEmpty(filter.Number) || p.Number.Contains(filter.Number)) &&
                    (!filter.OrderDateFrom.HasValue || p.OrderDate >= filter.OrderDateFrom) &&
                    (!filter.OrderDateTo.HasValue || p.OrderDate <= filter.OrderDateTo) &&
                    (filter.ClientCodes == null || filter.ClientCodes.Contains(p.ClientCode)))
                .ToListAsync();

            return records;
        }
    }
}
