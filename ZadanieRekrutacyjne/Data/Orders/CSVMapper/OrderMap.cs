﻿using CsvHelper.Configuration;
using System.Globalization;
using ZadanieRekrutacyjne.Data.Orders.Models;

namespace ZadanieRekrutacyjne.Data.Orders.CSVMapper
{
    public sealed class OrderMap : ClassMap<Order>
    {
        public OrderMap()
        {
            Map(m => m.Number)
                .Name("Number")
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
            Map(m => m.ClientCode)
                .Name("ClientCode")
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
            Map(m => m.ClientName)
                .Name("ClientName")
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
            Map(m => m.OrderDate)
                .Name("OrderDate")
                .TypeConverterOption.Format("dd/MM/yyyy")
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
            Map(m => m.ShipmentDate)
                .Name("ShipmentDate")
                .TypeConverterOption.Format("dd/MM/yyyy");
            Map(m => m.Quantity)
                .Name("Quantity")
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
            Map(m => m.Confirmed)
                .Name("Confirmed")
                .TypeConverterOption.BooleanValues(false, false, "1", "0")
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
            Map(m => m.Value)
                .Name("Value").TypeConverterOption.NumberStyles(NumberStyles.AllowDecimalPoint)
                .TypeConverterOption.CultureInfo(CultureInfo.InvariantCulture)
                .Validate(member => !string.IsNullOrEmpty(member.Field?.ToString()));
        }
    }
}
