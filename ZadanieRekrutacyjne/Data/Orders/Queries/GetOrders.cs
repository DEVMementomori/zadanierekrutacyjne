﻿using MediatR;
using ZadanieRekrutacyjne.Data.Orders.Filters;
using ZadanieRekrutacyjne.Data.Orders.Models;

namespace ZadanieRekrutacyjne.Data.Orders.Queries
{
    public class GetOrders : IRequest<IEnumerable<Order>>
    {
        public OrdersFilter Filter { get; set; }

        public GetOrders(OrdersFilter filter)
        {
            Filter = filter;
        }
    }
}
