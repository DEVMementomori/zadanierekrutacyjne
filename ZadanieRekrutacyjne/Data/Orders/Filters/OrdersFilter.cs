﻿namespace ZadanieRekrutacyjne.Data.Orders.Filters
{
    public class OrdersFilter
    {
        public string Number { get; set; }
        public DateTime? OrderDateFrom { get; set; }
        public DateTime? OrderDateTo { get; set; }
        public List<string> ClientCodes { get; set; }
    }
}
