﻿using MediatR;
using ZadanieRekrutacyjne.Data.Orders.Models;
using ZadanieRekrutacyjne.Data.Orders.Queries;
using ZadanieRekrutacyjne.Services.Orders;

namespace ZadanieRekrutacyjne.Data.Orders.Handlers
{
    public class GetOrdersHandler : IRequestHandler<GetOrders, IEnumerable<Order>>
    {
        private readonly IOrderService _orderService;

        public GetOrdersHandler(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public async Task<IEnumerable<Order>> Handle(GetOrders request, CancellationToken cancellationToken)
        {
            return await _orderService.GetOrdersAsync(request.Filter);
        }
    }
}
