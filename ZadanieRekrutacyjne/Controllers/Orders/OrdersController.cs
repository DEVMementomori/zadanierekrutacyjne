﻿using Microsoft.AspNetCore.Mvc;
using MediatR;
using ZadanieRekrutacyjne.Data.Orders.Filters;
using ZadanieRekrutacyjne.Data.Orders.Models;
using ZadanieRekrutacyjne.Data.Orders.Queries;

namespace ZadanieRekrutacyjne.Controllers.Orders
{
    [ApiController]
    [Route("api/orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrdersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<Order>> Get([FromQuery] OrdersFilter filter)
        {
            return await _mediator.Send(new GetOrders(filter));
        }
    }
}
